from datasets import load_dataset
from transformers import (
    AutoTokenizer, DataCollatorForTokenClassification,
    AutoModelForTokenClassification,
    AdamW
)
from transformers.modeling_outputs import (
    TokenClassifierOutput
)
from typing import List, Optional, Dict
import yaml
from pathlib import Path
import json
import torch
from torch.utils.data import (
    DataLoader
)
from tqdm import tqdm
from torchmetrics.classification import (
    MulticlassAccuracy, MulticlassPrecision,
    MulticlassRecall, MulticlassF1Score
)


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


Path(config["output_dir"]).mkdir(parents=True, exist_ok=True)
device = torch.device(config["device"])


dataset = load_dataset(
    path=config["dataset_path"]
)


label_to_id: Dict[str, int] = {}
for instance in dataset["train"]:
    for tag in instance["tags"]:
        if tag not in label_to_id:
            label_to_id[tag] = len(label_to_id)
with open(
        f"{config['output_dir']}/label_to_id.json", mode="w"
) as file_obj:
    json.dump(label_to_id, file_obj)


def get_aligned_labels(
        word_ids: List[int], labels: List[str]
) -> List[int]:
    """
    Get aligned labels
    :param word_ids: word ids of tokens
    :param labels: list of labels
    :return: aligned labels
    """
    aligned_labels: List[int] = []
    current_word_id: Optional[int] = None
    for word_id in word_ids:
        if word_id != current_word_id:
            # start new word
            aligned_labels.append(
                -100 if word_id is None else label_to_id[labels[word_id]]
            )
            current_word_id = word_id
        elif word_id is None:
            # special token
            aligned_labels.append(-100)
        else:
            label: str = labels[word_id]
            if label.startswith("B"):
                label = "I" + label[1:]
            aligned_labels.append(label_to_id[label])

    return aligned_labels


tokenizer = AutoTokenizer.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model_name"],
    use_fast=True, add_prefix_space=True
)


def tokenize_and_align_labels(data: Dict) -> Dict:
    """
    Tokenize and align labels
    :param data: data instance
    :return: dict contains
        - input_ids
        - attention_mask
        - labels
    """
    inputs = tokenizer(
        data["words"], truncation=True,
        max_length=config["max_length"], is_split_into_words=True
    )
    labels = get_aligned_labels(
        word_ids=inputs.word_ids(), labels=data["tags"]
    )
    return {
        "input_ids": inputs["input_ids"],
        "attention_mask": inputs["attention_mask"],
        "labels": labels
    }


dataset = dataset.map(
    tokenize_and_align_labels, batched=False,
    remove_columns=dataset["train"].column_names,
)
data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)
train_dataloader = DataLoader(
    dataset=dataset["train"], batch_size=config["batch_size"],
    shuffle=True, collate_fn=data_collator
)
val_dataloader = DataLoader(
    dataset=dataset["validation"], batch_size=config["batch_size"],
    shuffle=False, collate_fn=data_collator
)


model = AutoModelForTokenClassification.from_pretrained(
    pretrained_model_name_or_path=config["pretrained_model_name"],
    num_labels=len(label_to_id)
).to(device)
optimizer = AdamW(
    params=model.parameters(), lr=config["learning_rate"]
)


def train_epoch(epoch: int, dataloader: DataLoader) -> float:
    """
    Execute an training epoch
    :param epoch:
    :param dataloader:
    :return:
    """
    model.train()
    step_losses: List[float] = []

    progress_bar = tqdm(
        iterable=enumerate(dataloader), desc=f"Training epoch {epoch}:  "
    )
    for step, batch in progress_bar:
        outputs: TokenClassifierOutput = model(
            input_ids=batch["input_ids"].to(device),
            attention_mask=batch["attention_mask"].to(device),
            labels=batch["labels"].to(device)
        )
        loss = outputs.loss

        loss.backward()
        if (
            (step + 1) % config["accumulate_gradient_batches"] == 0 or
            (step + 1) == len(dataloader)
        ):
            optimizer.step()
            optimizer.zero_grad()

        loss_value: float = loss.item()
        step_losses.append(loss_value)

        if (
                (step + 1) % config["steps_update_progress_bar"] == 0 or
                (step + 1) == len(dataloader)
        ):
            progress_bar.set_description(f"Training epoch {epoch}   loss {loss_value:.6f}:  ")
            progress_bar.update()
    progress_bar.close()

    return sum(step_losses) / len(step_losses)


accuracy_fn = MulticlassAccuracy(
    num_classes=len(label_to_id),
    ignore_index=-100
).to(device)
precision_fn = MulticlassPrecision(
    num_classes=len(label_to_id),
    ignore_index=-100
).to(device)
recall_fn = MulticlassRecall(
    num_classes=len(label_to_id),
    ignore_index=-100
).to(device)
f1_fn = MulticlassF1Score(
    num_classes=len(label_to_id),
    ignore_index=-100
).to(device)


@torch.no_grad()
def evaluate_epoch(epoch: int, dataloader: DataLoader) -> Dict[str, float]:
    """
    Execute evaluate model
    :param epoch: epoch to train
    :param dataloader: dataloader
    :return: accuracy, f1, precision, recall
    """
    model.eval()
    metric_to_values: Dict[str, List[float]] = {
        "accuracy": [],
        "precision": [],
        "recall": [],
        "f1": []
    }

    progress_bar = tqdm(
        iterable=enumerate(dataloader),
        desc=f"Evaluate epoch {epoch}:   "
    )
    for step, batch in progress_bar:
        outputs: TokenClassifierOutput = model(
            input_ids=batch["input_ids"].to(device),
            attention_mask=batch["attention_mask"].to(device)
        )

        predictions = outputs.logits.argmax(dim=-1)
        labels = batch["labels"].to(device)

        accuracy = accuracy_fn(predictions, labels)
        metric_to_values["accuracy"].append(accuracy.item())
        precision = precision_fn(predictions, labels)
        metric_to_values["precision"].append(precision.item())
        recall = recall_fn(predictions, labels)
        metric_to_values["recall"].append(recall.item())
        f1 = f1_fn(predictions, labels)
        metric_to_values["f1"].append(f1.item())

        if (
            (step + 1) % config["steps_update_progress_bar"] == 0 or
            (step + 1) == len(dataloader)
        ):
            progress_bar.set_description(
                f"Evaluate epoch {epoch}   accuracy {accuracy.item():.6f}   precision {precision.item():.6f}   recall {recall.item():.6f}    f1 {f1.item():.6f}:  "
            )
            progress_bar.update()

    progress_bar.close()

    return {
        key: sum(values) / len(values)
        for key, values in metric_to_values.items()
    }


best_f1: float = 0.0
for i in range(config["num_epochs"]):
    train_loss: float = train_epoch(
        epoch=i, dataloader=train_dataloader
    )
    print(f"Finish training, loss: {train_loss:.6f}")

    val_metric_to_value: Dict[str, float] = evaluate_epoch(
        epoch=i, dataloader=val_dataloader
    )
    print(f"Finish evaluate:    ", end="")
    for metric, value in val_metric_to_value.items():
        print(f"{metric}: {value:.6f}    ", end="")
    print("\n")

    if val_metric_to_value["f1"] > best_f1:
        print(f"F1 improve from {best_f1:.6f} to {val_metric_to_value['f1']:.6f}")
        best_f1 = val_metric_to_value["f1"]
        torch.save(model.state_dict(), f"{config['output_dir']}/model.pt")

