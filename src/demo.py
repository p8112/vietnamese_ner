from transformers import (
    AutoModelForTokenClassification,
    AutoTokenizer, PreTrainedTokenizer
)
from transformers.modeling_outputs import (
    TokenClassifierOutput
)
from typing import Dict, List, Tuple
import yaml
import torch
import json
import streamlit as st
import torch.nn as nn
import pandas as pd
from nltk.tokenize import wordpunct_tokenize


st.set_page_config(
    page_title="Vietnamese NER", layout="centered"
)


st.title("Vietnamese NER demo")
st.markdown("---")


@st.cache_resource
def load_config() -> Dict:
    """
    Load config
    :return:
    """
    with open("config.yaml", mode="r") as file_obj:
        result: Dict = yaml.safe_load(file_obj)
    return result


config = load_config()
device = torch.device(config["device"])


@st.cache_resource
def load_label_to_id() -> Dict[str, int]:
    """
    Load label to id
    :return:
    """
    with open(
            f"{config['output_dir']}/label_to_id.json", mode="r"
    ) as file_obj:
        result: Dict[str, int] = json.load(file_obj)
    return result


label_to_id: Dict[str, int] = load_label_to_id()
id_to_label: Dict[int, str] = {
    idx: label for label, idx in label_to_id.items()
}


@st.cache_resource
def load_model() -> nn.Module:
    """
    Load model
    :return:
    """
    result = AutoModelForTokenClassification.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model_name"],
        num_labels=len(label_to_id)
    ).to(device)
    result.load_state_dict(
        torch.load(f"{config['output_dir']}/model.pt", map_location=device)
    )
    result.eval()
    return result


@st.cache_resource
def load_tokenizer() -> PreTrainedTokenizer:
    """
    Load tokenizer
    :return:
    """
    result = AutoTokenizer.from_pretrained(
        pretrained_model_name_or_path=config["pretrained_model_name"],
        use_fast=True, add_prefix_space=True
    )
    return result


st.subheader("Enter a sentence:")
text: str = st.text_area(label="")
st.markdown("---")


if len(text) > 0:
    model = load_model()
    tokenizer = load_tokenizer()

    words: List[str] = wordpunct_tokenize(text)
    inputs = tokenizer(
        words, truncation=True,
        max_length=config["max_length"], is_split_into_words=True
    )

    with torch.no_grad():
        outputs: TokenClassifierOutput = model(
            input_ids=torch.tensor(inputs["input_ids"], dtype=torch.long).unsqueeze(dim=0).to(device),
            attention_mask=torch.tensor(inputs["attention_mask"], dtype=torch.float).unsqueeze(dim=0).to(device)
        )
        predictions = outputs.logits.argmax(dim=-1)[0]
        predictions = predictions.cpu().detach().tolist()
        predictions = [id_to_label[idx] for idx in predictions]

    words_labels: List[Tuple[str, str]] = []
    last_b_word_id: int = None
    last_i_word_id: int = None
    last_label: str = None
    for predict, word_id in zip(predictions, inputs.word_ids()):
        if predict.startswith("B"):
            # start a new entity
            if last_b_word_id is not None:
                words_labels.append(
                    (
                        " ".join(words[last_b_word_id:last_i_word_id+1]),
                        last_label
                    )
                )
            last_b_word_id = word_id
            last_i_word_id = word_id
            last_label = predict[2:]
        elif predict.startswith("I"):
            # continue an entity
            if last_b_word_id is None:
                # not start yet => invalid predict, ignore
                continue
            elif (
                last_label != predict[2:]
            ):
                # label is not the same => invalid predict, truncate
                if last_b_word_id is not None:
                    words_labels.append(
                        (
                            " ".join(words[last_b_word_id:last_i_word_id + 1]),
                            last_label
                        )
                    )
                last_b_word_id = None
                last_i_word_id = None
                last_label = None
            else:
                # append to currently predict
                last_i_word_id = word_id
        elif (
            word_id is not None and
            last_i_word_id is not None and
            word_id - last_i_word_id > config["max_tokens_between_b_i"]
        ):
            # exceed number of tokens between b and i
            if last_b_word_id is not None:
                words_labels.append(
                    (
                        " ".join(words[last_b_word_id:last_i_word_id + 1]),
                        last_label
                    )
                )
            last_b_word_id = None
            last_i_word_id = None
            last_label = None

    if last_b_word_id is not None:
        words_labels.append(
            (
                " ".join(words[last_b_word_id:last_i_word_id + 1]),
                last_label
            )
        )

    st.subheader("Predict result")
    df = pd.DataFrame(
        {
            "Label": [label for _, label in words_labels],
            "Word": [word for word, _ in words_labels]
        }
    )
    st.table(df)
